FROM python:3.7

MAINTAINER "AmmadKhalid" "ammadkhalid12@gmail.com"

RUN apt-get -y update
RUN apt-get install -y build-essential gcc musl-dev python3-dev libffi-dev screen libass-dev

# set display port to avoid crash
ENV DISPLAY=:99
# for flushing out print commands immedately.
ENV PYTHONUNBUFFERED=1
ENV BOT /bot/

# install tor and enable service on run
RUN apt-get install -y tor
RUN service tor start

RUN mkdir /bot
WORKDIR bot
COPY . /bot


COPY requirements.txt .
RUN apt-get update && apt-get install -y screen gcc libffi-dev libmariadb-dev \
  && pip install --upgrade pip \
  && pip install -r requirements.txt \

## quckly setup remote mysql.
```
docker run --name=test-mysql \
--env="MYSQL_ROOT_PASSWORD=#takeOver123" \
--env="MYSQL_ROOT_HOST=%" \
--volume=/opt/mysql_data:/var/lib/mysql \
-p 3306:3306 -d mysql/mysql-server
```

login into mysql and change password.
```
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '#takeOver123';
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '#takeOver123';
FLUSH PRIVILEGES
```

## put your info in .env file

`album.csv` and `artist.csv` are going to be in `results` directory.
same goes for database. use mysql.


## grab docker ip of proxy runner
run:
```
docker-compose ps 
```

```
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' _runner_proxy_1
```

### set the returned ip in docker env variable.
```
export http_proxy=172.21.0.2:5566
export https_proxy=172.21.0.2:5566
```

## to run:
```
scrapy crawl music_spider
```

if you wanted to leave it running in background use screen.
# -*- coding: utf-8 -*-
import dataset
from scrapy import signals
from scrapy.exporters import CsvItemExporter

from itunes.items import AlbumItem, SongItem


class ItunesPipeline(object):
    album_file_path = "results/album.csv"
    artist_file_path = "results/artist.csv"

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        self.album_file = open(self.album_file_path, 'w+b')
        self.artist_file = open(self.artist_file_path, 'w+b')
        self.album_exporter = CsvItemExporter(self.album_file)
        self.artist_exporter = CsvItemExporter(self.artist_file)
        self.album_exporter.start_exporting()
        self.artist_exporter.start_exporting()

    def spider_closed(self, spider):
        self.album_exporter.finish_exporting()
        self.artist_exporter.finish_exporting()
        self.artist_file.close()

    def process_item(self, item, spider):
        if isinstance(item, AlbumItem):
            self.album_exporter.export_item(item)
        elif isinstance(item, SongItem):
            self.artist_exporter.export_item(item)
        return item


class DatabasePipeLine:

    @property
    def album_table(self):
        return self.db['albums']

    @property
    def album_track_table(self):
        return self.db['album_tracks']

    def store_album_items(self, item: AlbumItem):
        self.album_table.insert(item)

    def store_album_track_items(self, item: SongItem):
        self.album_track_table.insert(item)

    def __init__(self, host, port, user, password, database):
        self.db = dataset.connect(
            f"mysql://{user}:{password}@{host}:{port}/{database}?charset=utf8mb4"
        )
        # auto create table columns.

    @classmethod
    def from_crawler(cls, crawler):
        db_settings = crawler.settings.getdict("DB_SETTINGS")
        pipeline = cls(db_settings['host'], db_settings['port'], db_settings['user'], db_settings['password'],
                       db_settings['db'])

        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        pass

    def spider_closed(self, spider):
        del self.db

    def process_item(self, item, spider):
        if isinstance(item, AlbumItem):
            self.store_album_items(item)
        elif isinstance(item, SongItem):
            self.store_album_track_items(item)
        return item
